require 'pathname'

module FirebaseCloudMessenger
  class AuthClient
    attr_reader :credentials_path, :credentials_json

    AUTH_SCOPE = "https://www.googleapis.com/auth/firebase.messaging".freeze

    def initialize(credentials_path = nil, credentials_json = nil)
      @credentials_path = credentials_path
      @credentials_json = credentials_json

      raise_credentials_not_supplied if !credentials_supplied?

      @authorizer = Google::Auth::ServiceAccountCredentials.make_creds(cred_args)
    end

    def fetch_access_token_info
      authorizer.fetch_access_token!
    end

    private

    attr_reader :authorizer

    def credentials_supplied?
      credentials_path || credentials_json || (ENV['GOOGLE_CLIENT_EMAIL'] && ENV['GOOGLE_PRIVATE_KEY'])
    end

    def raise_credentials_not_supplied
      msg = "Either a path to a service account credentials json "\
            "or a string with service account credentials json must be supplied,"\
            "or the `GOOGLE_CLIENT_EMAIL` and `GOOGLE_PRIVATE_KEY` env vars must be set."

      raise ArgumentError, msg
    end

    def cred_args
      args = { scope: AUTH_SCOPE }

      if !credentials_path.nil?
        credentials_io = Pathname.new(credentials_path)
      elsif !credentials_json.nil?
        credentials_io = StringIO.new(credentials_json)
      else
        return args
      end

      args.merge(json_key_io: credentials_io)
    end
  end
end
